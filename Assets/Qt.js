QUnit.test('Testing Multiplication function with four sets of inputs', function (assert) {
    assert.throws(function () { billing(); }, new Error("only numbers are allowed"), 'Passing in array correctly raises an Error');
    assert.strictEqual(billing(10,20), 200, 'Both are positive numbers');
    assert.strictEqual(billing(10,-20), -200, 'one is Positive and other is negative number');
    assert.strictEqual(billing(-10,-20), 200, 'both are negative numbers');
});
